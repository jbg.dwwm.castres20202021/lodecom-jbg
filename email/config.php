<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require_once 'email/vendor/autoload.php';

$loader = new \Twig\Loader\FilesystemLoader('email/vue');
$twig = new \Twig\Environment($loader, [
	'debug' => true,
	'cache'=> false
	]);

?>